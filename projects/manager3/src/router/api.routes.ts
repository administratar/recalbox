/**
 * @author Nicolas TESSIER aka Asthonishia
 */
export const ARCHITECTURE = '/architecture';

export const BIOS = {
  root: '/bios',
  all: '/bios',
  export: '/bios/download',
  upload: '/bios/upload/',
};

export const CONFIGURATION = {
  audio: '/configuration/audio',
  autorun: '/configuration/autorun',
  controllers: '/configuration/controllers',
  emustation: '/configuration/emulationstation',
  global: '/configuration/global',
  hat: '/configuration/hat',
  hyperion: '/configuration/hyperion',
  kodi: '/configuration/kodi',
  music: '/configuration/music',
  patreon: 'configuration/patron',
  scraper: '/configuration/scraper',
  screenshots: '/configuration/screenshots',
  system: '/configuration/system',
  updates: '/configuration/updates',
  wifi: '/configuration/wifi',
  wifi2: '/configuration/wifi2',
  wifi3: '/configuration/wifi3',
};

export const SYSTEM = {
  es: {
    start: '/system/frontend/start',
    stop: '/system/frontend/stop',
    restart: '/system/frontend/restart',
  },
  reboot: '/system/reboot',
  shutdown: '/system/shutdown',
  supportArchive: '/system/supportarchive',
};

export const THREEDO = '/configuration/3do';
export const SIXTYFOURDD = '/configuration/64dd';
export const AMIGA600 = '/configuration/amiga600';
export const AMIGA1200 = '/configuration/amiga1200';
export const AMIGACD32 = '/configuration/amigacd32';
export const AMIGACDTV = '/configuration/amigacdtv';
export const AMSTRADCPC = '/configuration/amstradcpc';
export const APPLE2 = '/configuration/apple2';
export const APPLE2GS = '/configuration/apple2gs';
export const ARDUBOY = '/configuration/arduboy';
export const ATARI800 = '/configuration/atari800';
export const ATARI2600 = '/configuration/atari2600';
export const ATARI5200 = '/configuration/atari5200';
export const ATARI7800 = '/configuration/atari7800';
export const ATARIST = '/configuration/atarist';
export const ATOMISWAVE = '/configuration/atomiswave';
export const BBCMICRO = '/configuration/bbcmicro';
export const BK = '/configuration/bk';
export const C64 = '/configuration/c64';
export const CDI = '/configuration/cdi';
export const CHANNELF = '/configuration/channelf';
export const COLECOVISION = '/configuration/colecovision';
export const DAPHNE = '/configuration/daphne';
export const DOS = '/configuration/dos';
export const DRAGON = '/configuration/dragon';
export const DREAMCAST = '/configuration/dreamcast';
export const EASYRPG = '/configuration/easyrpg';
export const FBNEO = '/configuration/fbneo';
export const FDS = '/configuration/fds';
export const GAMECUBE = '/configuration/gamecube';
export const GAMEGEAR = '/configuration/gamegear';
export const GB = '/configuration/gb';
export const GBA = '/configuration/gba';
export const GBC = '/configuration/gbc';
export const GW = '/configuration/gw';
export const GX4000 = '/configuration/gx4000';
export const INTELLIVISION = '/configuration/intellivision';
export const JAGUAR = '/configuration/jaguar';
export const LOWRESNX = '/configuration/lowresnx';
export const LUTRO = '/configuration/lutro';
export const LYNX = '/configuration/lynx';
export const MACINTOSH = '/configuration/macintosh';
export const MAME = '/configuration/mame';
export const MASTERSYSTEM = '/configuration/mastersystem';
export const MEGADRIVE = '/configuration/megadrive';
export const MEGADUCK = '/configuration/megaduck';
export const MODEL3 = '/configuration/model3';
export const MOONLIGHT = '/configuration/moonlight';
export const MSX1 = '/configuration/msx1';
export const MSX2 = '/configuration/msx2';
export const MSXTURBOR = '/configuration/msxturbor';
export const MULTIVISION = '/configuration/multivision';
export const N64 = '/configuration/n64';
export const NAOMI = '/configuration/naomi';
export const NAOMIGD = '/configuration/naomigd';
export const NDS = '/configuration/nds';
export const NEOGEO = '/configuration/neogeo';
export const NEOGEOCD = '/configuration/neogeocd';
export const NES = '/configuration/nes';
export const NGP = '/configuration/ngp';
export const NGPC = '/configuration/ngpc';
export const O2EM = '/configuration/o2em';
export const OPENBOR = '/configuration/openbor';
export const ORICATMOS = '/configuration/oricatmos';
export const PALM = '/configuration/palm';
export const PC88 = '/configuration/pc88';
export const PC98 = '/configuration/pc98';
export const PCENGINE = '/configuration/pcengine';
export const PCENGINECD = '/configuration/pcenginecd';
export const PCFX = '/configuration/pcfx';
export const PCV2 = '/configuration/pcv2';
export const PICO8 = '/configuration/pico8';
export const POKEMINI = '/configuration/pokemini';
export const PORTS = '/configuration/ports';
export const PSP = '/configuration/psp';
export const PS2 = '/configuration/ps2';
export const PSX = '/configuration/psx';
export const SAMCOUPE = '/configuration/samcooupe';
export const SATELLAVIEW = '/configuration/satellaview';
export const SATURN = '/configuration/saturn';
export const SCUMMVM = '/configuration/scummvm';
export const SCV = '/configuration/scv';
export const SEGA32X = '/configuration/sega32x';
export const SEGACD = '/configuration/segacd';
export const SG1000 = '/configuration/sg1000';
export const SNES = '/configuration/snes';
export const SOLARUS = '/configuration/solarus';
export const SPECTRAVIDEO = '/configuration/spectravideo';
export const SUFAMI = '/configuration/sufami';
export const SUPERGRAFX = '/configuration/supergrafx';
export const SUPERVISION = '/configuration/supervision';
export const THOMSON = '/configuration/thomson';
export const TI994A = '/configuration/ti994a';
export const TIC80 = '/configuration/tic80';
export const TRS80COCO = '/configuration/trs80coco';
export const UZEBOX = '/configuration/uzebox';
export const VECTREX = '/configuration/vectrex';
export const VG5000 = '/configuration/vg5000';
export const VIC20 = '/configuration/vic20';
export const VIDEOPACPLUS = '/configuration/videopacplus';
export const VIRTUALBOY = '/configuration/virtualboy';
export const WASM4 = '/configuration/wasm4';
export const WII = '/configuration/wii';
export const WSWAN = '/configuration/wswan';
export const WSWANC = '/configuration/wswanc';
export const X1 = '/configuration/x1';
export const X68000 = '/configuration/x68000';
export const ZMACHINE = '/configuration/zmachine';
export const ZX81 = '/configuration/zx81';
export const ZXSPECTRUM = '/configuration/zxspectrum';

export const MEDIA = {
  root: '/media',
  all: '/media',
  get: '/media/screenshot/',
  delete: '/media/',
  takeScreenshot: '/media/takescreenshot',
};

export const MONITORING = {
  cpuInfo: '/monitoring/cpuinfo',
  storageInfo: '/monitoring/storageinfo',
};

export const ROMS = {
  root: '/roms',
  all: '/roms',
};

export const SYSTEMS = {
  root: '/systems',
  all: '/systems',
};

export const TWITCH = {
  auth: 'https://id.twitch.tv/oauth2/token',
  schedule: 'https://api.twitch.tv/helix/schedule',
};

export const VERSIONS = '/versions';
